//  Created by Rui Hong on 2016-03-15.
//  Copyright © 2016 RH. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <random>
#include <map>
#include <set>
#include <vector>
#include <queue>
#include <string>

using namespace std;


// 0. Define Event
struct event {
    string type;
    double time;
    bool isError;
    int sequence_number;
} ;

// 1.1 Implement Channel
string channel (int bytes, double BER) {
    int bits = bytes * 8;
    int determinator [bits];
    // error simulator indicated in lab manual
    for (int i = 0; i < bits; i++) {
        int random_num = rand() % (int(1/BER)+1);
        if (random_num == 1) {
            // error bit
            determinator[i] = 0;
        } else {
            determinator[i] = 1;
        }
    }
    
    int counter = 0;
    for (int j = 0; j < bits; j++) {
        if (determinator[j] == 0) {
            counter++;
        }
    }
    
    if (counter == 0) {
        return "correct";
    } else if (counter >=5) {
        return "loss";
    } else {
        return "error";
    }
}

// 1. Implement the SEND()
event SEND(int RN, double BER, int &NEF, double delay, double current_time, int &numop) {
    int header = 54;
    int length = 1500;
    int C = 5*pow(10, 6);
    double backward_transfer_time = header*8/(double)C;

    if (BER == 0) {
        NEF = (NEF + 1) % 2;
        numop--;
        event ack = {"ACK", current_time+delay+backward_transfer_time, false, NEF};
        return ack;
    } else {
        // 1.1 Implement Channel
        // 1.1.1 Forward Channel
        string f_status = channel(header+length, BER);
        if (f_status == "loss") {
            // if packet lost, ack with type "null" means no actual ACK
            event ack = {.type = "null"};
            return ack;
        } else if (f_status == "error") {
            // 1.1.2 Backward Channel
            string b_status = channel(header, BER);
            if (b_status == "loss") {
                // if ACK lost, ack with type "null" means no actual ACK
                event ack = {.type = "null"};
                return ack;
            } else {
                event ack = {"ACK", current_time+delay+backward_transfer_time, true, NEF};
                return ack;
            }
        } else {
            // Correct packet received
            if (RN == NEF) {
                NEF = (NEF + 1) % 2;
                // Reduce the amount of packets left to transfer
                numop--;
            }
            // 1.1.2 Backward Channel
            string b_status = channel(header, BER);
            if (b_status == "loss") {
                event ack = {.type = "null"};
                return ack;
            } else if (b_status == "error") {
                event ack = {"ACK", current_time+delay+backward_transfer_time, true, NEF};
                return ack;
            } else {
                event ack = {"ACK", current_time+delay+backward_transfer_time, false, NEF};
                return ack;
            }
        }
    }
}

double startSender(double ratio, double BER, double delay) {
    // 2. Implement the Sender
    int num_of_packets = 10000;
    int SN = 0;
    int NEXT_EXPECTED_ACK = 1;
    double current_time = 0;
    double timeout = 0;
    
    // 3. Implement the Receiver
    int NEXT_EXPECTED_FRAME = 0;
    
    int header = 54;
    int length = 1500;
    int C = 5*pow(10, 6);
    double forward_transfer_time = (header + length)*8/(double)(C);
    
    // 2.1 Event Scheduler
    vector<event> ES;
    
    // 2.2 Next Event Processor
    while (num_of_packets > 0) {
        // if the event scheduler is empty, send the first packet
        if (ES.empty()) {
            current_time += forward_transfer_time;
            timeout = current_time + delay/2*ratio;
            event to = {.type = "timeout", .time = timeout};
            ES.push_back(to);
        } else {
            // normally, read the earliest event from event processor and remove it from ES
            event next;
            // initialize event_time to be the max value of double
            double event_time = 0x1.fffffffffffffp+1023;
            for (event e : ES) {
                if (e.time < event_time) {
                    event_time = e.time;
                }
            }
            // get the position of the event to be removed
            vector<event>::iterator position;
            for (position = ES.begin(); position != ES.end(); position++) {
                if (((event)*position).time == event_time) {
                    next = *position;
                    break;
                }
            }
            
            ES.erase(position);
            
            // Update current time
            current_time = next.time;
            
            // Send new packet when Correct ACK is received
            if (next.type == "ACK") {
                if (next.sequence_number == NEXT_EXPECTED_ACK && next.isError == false) {
                    SN = (SN + 1) % 2;
                    NEXT_EXPECTED_ACK = (NEXT_EXPECTED_ACK + 1) % 2;
                } else {
                    // skip the NAK
                    continue;
                }
            }
            
            // Register new timeout since a packet needs to be sent, either a new one or the old same packet
            current_time += forward_transfer_time;
            timeout = current_time + delay/2*ratio;
            for (event &e : ES) {
                if (e.type == "timeout") {
                    e.time = timeout;
                    break;
                }
            }
        }
        
        if (num_of_packets > 0) {
            event sent = SEND(SN, BER, NEXT_EXPECTED_FRAME, delay, current_time, num_of_packets);
            if (sent.type != "null") {
                ES.push_back(sent);
            }
        }
    }
    
    return 10000*8*length/current_time;
}


int main(int argc, const char * argv[]) {
    
    ofstream data;
    data.open("/Users/GraceHong/Desktop/WEB2/ABP.csv");
    double ratio = 2.5;
    while (ratio <= 12.5) {
        data << ratio << "," << startSender(ratio, 0, 0.01) << ","
                             << startSender(ratio, pow(10.0, -5), 0.01) << ","
                             << startSender(ratio, pow(10.0, -4), 0.01) << ","
                             << startSender(ratio, 0, 0.5) << ","
                             << startSender(ratio, pow(10.0, -5), 0.5) << ","
                             << startSender(ratio, pow(10.0, -4), 0.5) << ",\n";
        ratio += 2.5;
    }
    data.close();

    return 0;
}















