//  Created by Rui Hong on 2016-03-15.
//  Copyright © 2016 RH. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <random>
#include <map>
#include <set>
#include <vector>
#include <queue>
#include <string>

using namespace std;


// 0. Define Event
struct event {
    string type;
    double time;
    bool isError;
    int sequence_number;
} ;

// 1.1 Implement Channel
string channel (int bytes, double BER) {
    int bits = bytes * 8;
    int determinator [bits];
    // error simulator indicated in lab manual
    for (int i = 0; i < bits; i++) {
        int random_num = rand() % (int(1/BER)+1);
        if (random_num == 1) {
            // error bit
            determinator[i] = 0;
        } else {
            determinator[i] = 1;
        }
    }
    
    int counter = 0;
    for (int j = 0; j < bits; j++) {
        if (determinator[j] == 0) {
            counter++;
        }
    }
    
    if (counter == 0) {
        return "correct";
    } else if (counter >=5) {
        return "loss";
    } else {
        return "error";
    }
}

// 1. Implement the SEND()
event SEND(int RN, double BER, int &NEF, double delay, double current_time, int &numop) {
    int header = 54;
    int length = 1500;
    int C = 5*pow(10, 6);
    int size = 4;
    double backward_transfer_time = header*8/(double)C;

    if (BER == 0) {
        NEF = (NEF + 1) % (size + 1);
        numop--;
        event ack = {"ACK", current_time+delay+backward_transfer_time, false, NEF};
        return ack;
    } else {
        // 1.1 Implement Channel
        // 1.1.1 Forward Channel
        string f_status = channel(header+length, BER);
        if (f_status == "loss") {
            // if packet lost, ack with type "null" means no actual ACK
            event ack = {.type = "null"};
            return ack;
        } else if (f_status == "error") {
            // 1.1.2 Backward Channel
            string b_status = channel(header, BER);
            if (b_status == "loss") {
                // if ACK lost, ack with type "null" means no actual ACK
                event ack = {.type = "null"};
                return ack;
            } else {
                event ack = {"ACK", current_time+delay+backward_transfer_time, true, NEF};
                return ack;
            }
        } else {
            // Correct packet received
            if (RN == NEF) {
                NEF = (NEF + 1) % (size + 1);
                // Reduce the amount of packets left to transfer
                numop--;
            }
            // 1.1.2 Backward Channel
            string b_status = channel(header, BER);
            if (b_status == "loss") {
                event ack = {.type = "null"};
                return ack;
            } else if (b_status == "error") {
                event ack = {"ACK", current_time+delay+backward_transfer_time, true, NEF};
                return ack;
            } else {
                event ack = {"ACK", current_time+delay+backward_transfer_time, false, NEF};
                return ack;
            }
        }
    }
}

double startSender(double ratio, double BER, double delay) {
    // 2. Implement the Sender
    int num_of_packets = 10000;
    double current_time = 0;
    double timeout = 0;
    
    // Implement the buffer
    int size = 4;
    double T[size];
    // p points to the oldest not acked packet
    int p = 0;
    // sent points to the last sent packet
    int sent = 0;
    
    int NEXT_EXPECTED_ACK[size];
    for (int i = 0; i < size; i++) {
        NEXT_EXPECTED_ACK[i] = i + 1;
    }
    
    int SN[size];
    for (int i = 0; i < size; i++) {
        SN[i] = i;
    }
    
    // 3. Initialize packet properties
    int NEXT_EXPECTED_FRAME = 0;
    
    int header = 54;
    int length = 1500;
    int C = 5*pow(10, 6);
    double forward_transfer_time = (header + length)*8/(double)(C);
    
    // 2.1 Event Scheduler
    vector<event> ES;
    
    // 2.2 Next Event Processor
    while (num_of_packets > 0) {
        // if the event scheduler is empty, fill out the buffer
        if (ES.empty()) {
            // first time filling out the buffer
            for (int i = 0; i < size; i++) {
                if (i == 0) {
                    T[i] = current_time + forward_transfer_time;
                    current_time += forward_transfer_time;
                    timeout = current_time + delay/2*ratio;
                    event to = {.type = "timeout", .time = timeout};
                    ES.push_back(to);
                } else {
                    T[i] = T[i-1] + forward_transfer_time;
                }
            }
        } else {
            event next = {.type = "null"};
            // initialize event_time to be the max value of double to get the time of the earliest event
            double event_time = 3.40282e+38;
            
            // check if there are any events happened during transmission of last packet
            bool happend = false;
            for (event e : ES) {
                if (e.time < event_time) {
                    event_time = e.time;
                }
                if (e.time < T[sent] && happend == false) {
                    happend = true;
                }
            }
            
            //   if events happened during transmission of last packet or if the last packet is the last one in buffer,
            // find the earlist event.
            if (happend || sent == size - 1 || sent == num_of_packets - 1) {
                vector<event>::iterator position;
                // first get the earliest event and remove it from ES
                for (position = ES.begin(); position != ES.end(); position++) {
                    if (((event)*position).time == event_time) {
                        next = *position;
                        break;
                    }
                }
                ES.erase(position);
            }
            
            
            if (next.type != "null") {
                // Update current time
                if (next.time < T[sent]) {
                    // current time needs to be equal to the finish time of last one
                    current_time = T[sent];
                } else {
                    current_time = next.time;
                }
                
                // check type of the event
                if (next.type == "timeout") {
                    ES.clear();
                    
                    // re-transmit all packets in buffer
                    sent = 0;
                    for (int i = 0; i < size; i++) {
                        if (i == 0) {
                            T[i] = current_time + forward_transfer_time;
                            current_time += forward_transfer_time;
                            timeout = current_time + delay/2*ratio;
                            event to = {.type = "timeout", .time = timeout};
                            ES.push_back(to);
                        } else {
                            T[i] = T[i-1] + forward_transfer_time;
                        }
                    }
                } else if (next.type == "ACK" && next.isError == false) {
                    // check if wrong ack #?
                    for (int ack = 0; ack < size; ack++) {
                        if (NEXT_EXPECTED_ACK[ack] == next.sequence_number) {
                            // check which one is acknowledged
                            int i = (next.sequence_number - p) % (size + 1);
                            if (i < 0) {
                                i = i + 5;
                            }
                            int move = i;
                            p = next.sequence_number;
                            
                            int num_to_remove = 0;
                            for (event e : ES) {
                                if (e.time < T[sent]) {
                                    num_to_remove++;
                                }
                            }
                            
                            while (num_to_remove > 0) {
                                vector<event>::iterator position;
                                for (position = ES.begin(); position != ES.end(); position++) {
                                    if (((event)*position).time < T[sent]) {
                                        break;
                                    }
                                }
                                ES.erase(position);
                                num_to_remove--;
                            }
                            
                            // slide the buffer
                            double temp_T[size];
                            int temp_SN[size];
                            for (int k = 0; k < (size - i); k++) {
                                temp_T[k] = T[k+i];
                                temp_SN[k] = SN[k+i];
                            }
                            
                            // put new packets into buffer
                            // time of first packet put in should current_time + forward_transfer_time
                            temp_T[size-i] = current_time + forward_transfer_time;
                            temp_SN[size-i] = (SN[size-1] + 1) % (size + 1);
                            i--;
                            while (i > 0) {
                                temp_T[size-i] = temp_T[size-i-1] + forward_transfer_time;
                                temp_SN[size-i] = (temp_SN[size-i-1] + 1) % (size + 1);
                                i--;
                            }
                            
                            // put value in temp array back
                            for (int i = 0; i < size; i++) {
                                T[i] = temp_T[i];
                                SN[i] = temp_SN[i];
                                NEXT_EXPECTED_ACK[i] = (SN[i] + 1) % (size + 1);
                            }
                            
                            // push new timeout
                            bool timeout = false;
                            for (event &e : ES) {
                                if (e.type == "timeout") {
                                    e.time = T[0] + delay/2*ratio;
                                    timeout = true;
                                    break;
                                }
                            }
                            if (!timeout) {
                                event to = {.type = "timeout", .time = T[0] + delay/2*ratio};
                                ES.push_back(to);
                            }
                            
                            sent = sent - move + 1;
                        }
                    }
                } else {
                    // if ack received with error and buffer is not full, send next packet
                    // if buffer is full, skip this event
                    if (sent < 3 && sent != num_of_packets - 1) {
                        sent++;
                    } else {
                        continue;
                    }
                }
            } else {
                // if no events happened and buffer not full, send next packet
                if (sent < 3 && sent != num_of_packets - 1) {
                    sent++;
                } else {
                    continue;
                }
            }
        }

        if (num_of_packets > 0) {
            event sent_packet = SEND(SN[sent], BER, NEXT_EXPECTED_FRAME, delay, T[sent], num_of_packets);
            if (sent_packet.type != "null") {
                ES.push_back(sent_packet);
            }
        }
    }
    
    ES.clear();
    return 10000*8*length/current_time;
}


int main(int argc, const char * argv[]) {
    
    ofstream data;
    data.open("/Users/GraceHong/Desktop/WEB2/GBN.csv");
    double ratio = 2.5;
    while (ratio <= 12.5) {
        data << startSender(ratio, 0, 0.01) << ","
             << startSender(ratio, pow(10.0, -5), 0.01) << ","
             << startSender(ratio, pow(10.0, -4), 0.01) << ","
             << startSender(ratio, 0, 0.5) << ","
             << startSender(ratio, pow(10.0, -5), 0.5) << ","
             << startSender(ratio, pow(10.0, -4), 0.5) << ",\n";
        ratio += 2.5;
    }
    data.close();

    return 0;
}















