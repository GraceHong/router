//
//  Created by Rui Hong on 2016-02-01.
//  Copyright © 2016 RH. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <random>
#include <map>
#include <set>
#include <vector>

double exponentialVariable(double parameter);
std::pair<double, double> DESK(double L, double C, double P, int k);
std::pair<double, double> DES(double L, double C, double P);

int main(int argc, const char * argv[]) {
    // insert code here...
    
    /*std::ofstream data;
    data.open("/Users/GraceHong/Desktop/WEB1/Q5k40.csv");
    data << "Utilization p, E[N] K = 40,\n";
    
    double p = 0.5;
    double step = 0.1;
    std::pair<double, double> result;
    while (p <= 1.6) {
        result = DESK(12000, 1000000, p, 40);
        data << p << "," << result.first << ",\n";
        p += step;
        std::cout << p << std::endl;
    }
    data.close();*/
    
    std::ofstream data;
    data.open("/Users/GraceHong/Desktop/WEB1/Q5_2k40.csv");
    data << "Utilization p, PLOSS K = 40,\n";
    
    double p = 0.4;
    std::pair<double, double> result;
    while (p < 10) {
        result = DESK(12000, 1000000, p, 40);
        data << p << "," << result.second << ",\n";
        if (p <= 2) {
            p += 0.1;
        } else if (p > 2 && p <= 5) {
            p += 0.2;
        } else {
            p += 0.4;
        }
        std::cout << p << std::endl;
    }
    data.close();
    
    return 0;
}

double exponentialVariable(double parameter) {
    double u = ((double) rand() / (double) (RAND_MAX)); // Uniform random variable from (0,1)
    return ((double)-1 / parameter)*log(1-u);
}

std::pair<double, double> DESK(double L, double C, double P, int k) {
    int duration = 10000; // Duration of simulation
    double lambda = C * P / L; // Parameter for Possion distribution of packet arrivals
    double alpha = 4 * lambda; // Parameter for Possion distribution of random observation
    
    // Event schedular
    std::multimap<double, char> ES = {};
    
    // 1. Generate a set of random observation times according to a Poisson
    // distribution with parameter α.
    double observationTime = 0;
    while (observationTime < duration) {
        observationTime += exponentialVariable(alpha);
        if (observationTime < duration) {
            ES.insert(std::pair<int, char>(observationTime, 'O'));
        }
    }
    
    // 2. Generate a set of packet arrivals (according to a Poisson distribution with parameter λ)
    // All arrival events should occur before the last observation events.
    double arrivalTime = 0;
    double departureTime = 0;
    std::vector<double> departureTimes; // Hold the list of departure times of packets that are not departured yet.
    double packetLength = 0;
    int numOfPacketsInQueue = 0;
    int numOfPacketsGen = 0;
    int numOfLoss = 0;
    int numOfDepartured = 0;
    
    while (arrivalTime < observationTime) {
        arrivalTime += exponentialVariable(lambda);
        if (arrivalTime < observationTime) {
            // Increase the number of generated packets by 1;
            numOfPacketsGen += 1;
            
            numOfDepartured = 0;
            
            std::vector<double> packetsDepartured;
            for (auto const& time: departureTimes) {
                if (time <= arrivalTime) {
                    packetsDepartured.push_back(time);
                }
            }
            for (auto const& dTime: packetsDepartured) {
                departureTimes.erase(std::remove(departureTimes.begin(), departureTimes.end(), dTime), departureTimes.end());
                numOfDepartured += 1;
            }
            
            packetsDepartured.erase(packetsDepartured.begin(), packetsDepartured.end());
            numOfPacketsInQueue -= numOfDepartured;
            
            // If the queue is full, drop the packet;
            if (numOfPacketsInQueue > k) {
                numOfLoss += 1;
                continue;
            }
            
            // Increase the number of packets in the queue by 1
            numOfPacketsInQueue += 1;
            
            // 3. Generate length for the packet (according to an exponential distribution with parameter 1/L)
            packetLength = exponentialVariable(1/(double)L);
            
            // calculate packets departure times based on the state of the system.
            if (arrivalTime >= departureTime) {
                departureTime = arrivalTime + packetLength / C;
            } else {
                departureTime = departureTime + packetLength / C;
            }
            
            departureTimes.push_back(departureTime);
            ES.insert(std::pair<double, char>(arrivalTime, 'A'));
            ES.insert(std::pair<double, char>(departureTime, 'D'));
        }
    }
    
    // 4. Now initialize the following variables to zero.
    int Na = 0; // Number of packets arrivals so far
    int Nd = 0; // Number of packets departures so far
    int No = 0; // Number of observations so far
    int totalPacketsInQ = 0; // Total number of packets that have been observed in queue.
    
    // 5. Sort the event schedular based on values
    std::set< std::pair<double, char> > sortedES(ES.begin(), ES.end());
    
    // 6. Deque events
    for( const auto& pair : sortedES ) {
        if (pair.second == 'A') {
            Na += 1;
        } else if (pair.second == 'O') {
            No += 1;
            int state = Na - Nd;
            totalPacketsInQ += state;
        } else {
            Nd += 1;
        }
    }
    
    return std::pair<double, double>((double)totalPacketsInQ / No, (double)numOfLoss / numOfPacketsGen);
}

std::pair<double, double> DES(double L, double C, double P) {
    int duration = 10000; // Duration of simulation
    double lambda = C * P / L; // Parameter for Possion distribution of packet arrivals
    double alpha = 4 * lambda; // Parameter for Possion distribution of random observation
    
    // Event schedular
    std::map<double, char> ES = {};
    
    // 1. Generate a set of random observation times according to a Poisson
    // distribution with parameter α.
    double observationTime = 0;
    while (observationTime < duration) {
        observationTime += exponentialVariable(alpha);
        if (observationTime < duration) {
            ES.insert(std::pair<int, char>(observationTime, 'O'));
        }
    }
    
    // 2. Generate a set of packet arrivals (according to a Poisson distribution with parameter λ)
    // All arrival events should occur before the last observation events.
    double arrivalTime = 0;
    double departureTime = 0;
    double packetLength = 0;
    
    while (arrivalTime < observationTime) {
        arrivalTime += exponentialVariable(lambda);
        if (arrivalTime < observationTime) {
            // 3. Generate length for the packet (according to an exponential distribution with parameter 1/L)
            packetLength = exponentialVariable(1/(double)L);
            
            // calculate packets departure times based on the state of the system.
            if (arrivalTime >= departureTime) {
                departureTime = arrivalTime + packetLength / C;
            } else {
                departureTime = departureTime + packetLength / C;
            }
            
            ES.insert(std::pair<double, char>(arrivalTime, 'A'));
            ES.insert(std::pair<double, char>(departureTime, 'D'));
        }
    }
    
    // 4. Now initialize the following variables to zero.
    int Na = 0; // Number of packets arrivals so far
    int Nd = 0; // Number of packets departures so far
    int No = 0; // Number of observations so far
    int totalPacketsInQ = 0; // Total number of packets that have been observed in queue.
    int numOfIdle = 0;  // Total number of times that the server is idle when observed.
    
    // 5. Sort the event schedular based on keys.
    std::set< std::pair<double, char> > sortedES(ES.begin(), ES.end());
    
    // 6. Deque events.
    for( const auto& pair : sortedES ) {
        if (pair.second == 'A') {
            Na += 1;
        } else if (pair.second == 'O') {
            No += 1;
            int state = Na - Nd;
            totalPacketsInQ += state;
            if (state == 0) {
                numOfIdle += 1;
            }
        } else {
            Nd += 1;
        }
    }
    
    return std::pair<double, double>((double)totalPacketsInQ / No, (double)numOfIdle / No);
}

